import { User } from '../../auth/models/user.interface';

export interface Team {
  teamName: string;
  id?: number;
  teamMembers: User[];
}
