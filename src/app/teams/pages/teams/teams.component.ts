import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { CreateEditTeamComponent } from '../../components/create-edit-team/create-edit-team.component';
import { TeamsService } from '../../services/teams.service';
import { Team } from '../../models/team.interface';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss'],
})
export class TeamsComponent implements OnInit, OnDestroy {
  teams: Array<Team>;
  destroy$ = new Subject<boolean>();

  constructor(
    private teamService: TeamsService,
    private matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getTeams();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  getTeams() {
    this.teamService
      .getAllTeams()
      .pipe(takeUntil(this.destroy$))
      .subscribe((teams) => (this.teams = teams));
  }

  openDialog(): void {
    const dialogRef = this.matDialog.open(CreateEditTeamComponent, {
      height: '400px',
      width: '400px',
    });
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe((result) => {
        if (result) {
          this.getTeams();
        }
      });
  }
}
