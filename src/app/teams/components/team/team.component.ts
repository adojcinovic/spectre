import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { takeUntil, Subject, Observable } from 'rxjs';
import { Team } from '../../models/team.interface';
import { TeamsService } from '../../services/teams.service';
import { CreateEditTeamComponent } from '../create-edit-team/create-edit-team.component';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss'],
})
export class TeamComponent implements OnDestroy {
  @Input() team: Team;
  @Output() deletedTeamEvent = new EventEmitter<Team>();

  destroy$ = new Subject<boolean>();

  constructor(
    private matDialog: MatDialog,
    private teamService: TeamsService
  ) {}

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  openEditDialog(): void {
    this.matDialog.open(CreateEditTeamComponent, {
      data: this.team,
      height: '400px',
      width: '400px',
    });
  }

  deleteTeam(): void {
    if (this.team.teamMembers.length) {
      this.team.teamMembers.forEach((member) => {
        member.teamMember = false;
        this.teamService
          .updateUser(member)
          .pipe(takeUntil(this.destroy$))
          .subscribe();
      });
    }
    this.teamService
      .deleteTeam(this.team.id)
      .pipe(takeUntil(this.destroy$))
      .subscribe();
    this.deletedTeamEvent.emit(this.team);
  }
}
