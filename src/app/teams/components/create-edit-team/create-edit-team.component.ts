import { Component, Inject, Input, OnDestroy, OnInit } from '@angular/core';
import { Observable, pipe, Subject, takeUntil } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FieldControl } from 'src/app/core/models/field-control.interface';
import { Team } from '../../models/team.interface';
import { TeamsService } from '../../services/teams.service';
import { User } from 'src/app/auth/models/user.interface';

@Component({
  selector: 'app-create-edit-team',
  templateUrl: './create-edit-team.component.html',
  styleUrls: ['./create-edit-team.component.scss'],
})
export class CreateEditTeamComponent implements OnInit, OnDestroy {
  destroy$ = new Subject<boolean>();

  teamForm: FormGroup;
  availableUsers: User[] = [];
  selectedUsers: User[] = [];
  removedUsers: User[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Team,
    private fb: FormBuilder,
    private teamsService: TeamsService
  ) {}

  ngOnInit(): void {
    this.teamForm = this.buildForm();
    this.getAvailableUsers();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  buildForm(): FormGroup {
    return this.fb.group({
      teamName: [
        this.data ? this.data.teamName : '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(20),
        ],
      ],
      teamMembers: [[]],
    });
  }

  get fields(): FieldControl {
    return this.teamForm.controls;
  }

  getAvailableUsers(): void {
    this.teamsService
      .getUsers()
      .pipe(takeUntil(this.destroy$))
      .subscribe((users) => {
        this.availableUsers = users.filter((user) => {
          return user.teamMember === false;
        });
      });
  }

  addToTeam(selectedUser: User): void {
    this.teamForm.value.teamMembers.push(selectedUser);
    this.availableUsers = this.availableUsers.filter((user) => {
      return user.id !== selectedUser.id;
    });
    selectedUser.teamMember = true;
    this.selectedUsers.push(selectedUser);
  }

  removeFromTeam(userToRemove: User): void {
    userToRemove.teamMember = false;
    this.removedUsers.push(userToRemove);
    this.availableUsers.push(userToRemove);
    this.data.teamMembers = this.data.teamMembers.filter((user) => {
      return userToRemove.id !== user.id;
    });
  }

  returnToTeam(): void {
    this.removedUsers.forEach((user) => {
      this.data.teamMembers.push(user);
    });
  }

  edit(): void {
    this.selectedUsers.forEach((user) => {
      this.data.teamMembers.push(user);
    });

    this.teamsService
      .updateTeam(this.data)
      .pipe(takeUntil(this.destroy$))
      .subscribe();

    if (this.removedUsers.length) {
      this.removedUsers.forEach((user) => {
        this.teamsService
          .updateUser(user)
          .pipe(takeUntil(this.destroy$))
          .subscribe();
      });
    }

    if (this.selectedUsers.length) {
      this.selectedUsers.forEach((user) => {
        this.teamsService
          .updateUser(user)
          .pipe(takeUntil(this.destroy$))
          .subscribe();
      });
    }
  }

  create(): void {
    this.teamsService
      .addNewTeam(this.teamForm.value)
      .pipe(takeUntil(this.destroy$))
      .subscribe();
    this.selectedUsers.forEach((user) => {
      this.teamsService
        .updateUser(user)
        .pipe(takeUntil(this.destroy$))
        .subscribe();
    });
  }

  save(): void {
    this.data ? this.edit() : this.create();
  }
}
