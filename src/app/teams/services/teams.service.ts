import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Team } from '../models/team.interface';
import { User } from 'src/app/auth/models/user.interface';
import { API_URL } from '../../core/constants/api-url';

@Injectable({
  providedIn: 'root',
})
export class TeamsService {
  constructor(private http: HttpClient) {}

  getAllTeams(): Observable<Team[]> {
    return this.http.get<Team[]>(`${API_URL}teams`);
  }

  addNewTeam(team: Team): Observable<Team> {
    return this.http.post<Team>(`${API_URL}teams`, team);
  }

  updateTeam(team: Team): Observable<Team> {
    return this.http.patch<Team>(`${API_URL}teams/${team.id}`, team);
  }

  deleteTeam(teamId: number | undefined): Observable<Team> {
    return this.http.delete<Team>(`${API_URL}teams/${teamId}`);
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${API_URL}users`);
  }

  updateUser(user: User): Observable<User> {
    return this.http.patch<User>(`${API_URL}users/${user.id}`, user);
  }
}
