import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { TeamsRoutingModule } from './teams-routing.module';
import { TeamComponent } from './components/team/team.component';
import { TeamsComponent } from './pages/teams/teams.component';
import { CreateEditTeamComponent } from './components/create-edit-team/create-edit-team.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [TeamComponent, TeamsComponent, CreateEditTeamComponent],
  imports: [
    CommonModule,
    TeamsRoutingModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    SharedModule,
  ],
})
export class TeamsModule {}
