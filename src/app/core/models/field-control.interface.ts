import { AbstractControl } from "@angular/forms";

export interface FieldControl {
  [key: string]: AbstractControl;
}
