export interface CustomValidatorType {
  [key: string]: boolean;
}
