import { AbstractControl } from '@angular/forms';
import { CustomValidatorType } from '../models/custom-validator.interface';

export function passwordValidator(
  control: AbstractControl
): CustomValidatorType | null {
  const password: AbstractControl | null =
    control.parent && control.parent?.get('password');
  const confirmPassword: AbstractControl | null =
    control.parent && control.parent?.get('confirmPassword');
  const validation =
    password && confirmPassword && password.value !== confirmPassword.value
      ? { notMatching: true }
      : null;

  return validation;
}
