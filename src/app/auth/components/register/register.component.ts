import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { passwordValidator } from '../../../core/utils/common-functions';
import { FieldControl } from '../../../core/models/field-control.interface';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit, OnDestroy {
  registerForm: FormGroup;

  destroy$ = new Subject<boolean>();

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  get inputs(): FieldControl {
    return this.registerForm.controls;
  }

  ngOnInit(): void {
    this.registerForm = this.buildForm();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  buildForm(): FormGroup {
    return this.fb.group(
      {
        username: [
          '',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(20),
          ],
        ],
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.pattern('^.{4,25}$')]],
        confirmPassword: ['', [Validators.required, passwordValidator]],
        teamMember: [false],
      },
      {
        validator: passwordValidator,
      }
    );
  }

  onSubmit(): void {
    this.authService
      .addUser(this.registerForm.value)
      .pipe(takeUntil(this.destroy$))
      .subscribe();
    this.router.navigate(['auth/login']);
  }
}
