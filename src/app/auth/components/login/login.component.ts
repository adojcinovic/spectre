import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { FieldControl } from '../../../core/models/field-control.interface';
import { AuthorizedUser } from '../../models/authorized-user.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  
  error: string;

  destroy$ = new Subject<boolean>();

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  get inputs(): FieldControl {
    return this.loginForm.controls;
  }

  ngOnInit(): void {
    this.loginForm = this.buildForm();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  buildForm(): FormGroup {
    return this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  onSubmit(): void {
    this.authService
      .loginUser(this.loginForm.value)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (user: AuthorizedUser) => {
          localStorage.setItem('token', user.accessToken);
          localStorage.setItem('user', JSON.stringify(user.user));
          this.router.navigate(['/teams']);
        },
        (error) => {
          this.error = error.error;
        }
      );
  }
}
