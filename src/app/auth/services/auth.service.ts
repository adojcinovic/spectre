import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { NewUser } from '../models/new-user.interface';
import { User } from '../models/user.interface';
import { Credentials } from '../models/credentials.interface';
import { API_URL } from '../../core/constants/api-url';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  addUser(user: NewUser): Observable<NewUser> {
    const registerUrl: string = `${API_URL}register`;
    return this.http.post<NewUser>(registerUrl, user);
  }

  loginUser(user: Credentials): Observable<any> {
    const loginUrl: string = `${API_URL}login`;
    return this.http.post<Credentials>(loginUrl, user);
  }

  logOut(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
  }

  getUsers(): Observable<User> {
    return this.http.get<User>(`${API_URL}/users`);
  }

  getToken(): string | null {
    return localStorage.getItem('token');
  }
}
