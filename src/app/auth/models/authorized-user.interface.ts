export interface AuthorizedUser {
  accessToken: string;
  user: {
    username: string;
    id: number;
    confirmPassword: string;
    email: string;
  };
}
