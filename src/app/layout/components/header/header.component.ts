import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  constructor(private authService: AuthService, private router: Router) {}

  loggingOut(): void {
    this.authService.logOut();
    this.router.navigate(['auth/login']);
  }

  checkToken(): boolean {
    return !!this.authService.getToken();
  }
}
